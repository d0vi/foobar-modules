package com.gitlab.d0vi.app;

import com.gitlab.d0vi.foo.Foo;
import com.gitlab.d0vi.bar.Bar;

public class App {
  public static void main(String[] args) {
  	System.out.println(new Foo().getFoo() + new Bar().getBar());
  }
}
