# Foobar Modules

Foobar Modules is just a minimalistic Java project that uses
Project Jigsaw Module System.

#### Requirements

- [Git](https://git-scm.com/).
- Java 9+: you can get it using your package manager or either downloading [some vendor
binaries](https://adoptopenjdk.net/).

#### Run the code

```
git clone
cd foobar-modules
./build.sh
```