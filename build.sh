#!/bin/bash

rm -rf output

mkdir -p output/mlib

javac -d output/classes --module-source-path src $(find src -name "*.java")

jar -c -f output/mlib/com.gitlab.d0vi.foo@1.0.jar -C output/classes/com.gitlab.d0vi.foo .
jar -c -f output/mlib/com.gitlab.d0vi.bar@1.0.jar -C output/classes/com.gitlab.d0vi.bar .
jar -c -f output/mlib/com.gitlab.d0vi.app@1.0.jar --main-class=com.gitlab.d0vi.app.App \
  -C output/classes/com.gitlab.d0vi.app .

rm -rf output/classes

java -p output/mlib -m com.gitlab.d0vi.app
